resource "aws_db_instance" "default" {
  allocated_storage    = 10
  engine               = "postgres"
  engine_version       = var.db_version
  instance_class       = "db.t3.micro"
  name                 = "mydb"
  username             = "foo"
  password             = "foobarbaz"
  parameter_group_name = aws_db_parameter_group.rdspg.name
  skip_final_snapshot  = true
}

resource "aws_db_parameter_group" "rdspg" {
  name   = "rds-pg"
  family = "postgres13"

  parameter {
    name  = "rds.force_ssl"
    value = var.ssl
  }
}